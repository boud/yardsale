## Yard-sale simulation for Chakraborti 2002 random wealth evolution model

##    Copyright (C) 2022 Boud Roukema GPL-3+
##
##    This program is free software; you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation; either version 3, or (at your option)
##    any later version.
##
##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with this program; if not, write to the Free Software Foundation,
##    Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
##
##    See also https://www.gnu.org/licenses/gpl.html

## avoid compilation of the function(s) in this file
## https://docs.octave.org/v6.2.0/Script-Files.html

## Sources:
## * McGhee, Michelle et al 2022 - very clear pop-science explanation - https://pudding.cool/2022/12/yard-sale
## * Chakraborti, Anirban 2002 - https://www.worldscientific.com/doi/10.1142/S0129183102003905
## * Boghosian, Bruce M. 2029 - affine wealth model https://www.scientificamerican.com/article/is-inequality-inevitable/
## * https://en.wikipedia.org/wiki/Bruce_M._Boghosian


1 # octave hack # leave this in for convenience

global cash # the bank's array of how much cash everybody has
global N_people = 100 # number of people
cash = 1000 .* ones(1,N_people); # start with e.g. $1000 each
global frac = 0.2 # maximum fraction to wager, e.g. 0.2 = 20%
global ran_frac = 0 # replace fixed frac by uniform random frac up to frac
global tax_frac = 0.005 # tax for redistribution, e.g. 0.005 = 0.5% is enough to block the megarich

##do_tax = 1  # ENable taxation
do_tax = 0 # DISable taxation

N_iterations = 10000

## This function updates a single random pair.
function result = update_pairs()
  global cash
  global N_people
  global frac
  global ran_frac
  ## first random integer - uniform integer from among N_people
  j1=randi(N_people);
  ## second random integer - uniform integer from among the N_people-1 values
  ## 1, ..., j1-1, j1+1, ..., N_people
  j2=randi(N_people-1);
  if(j2 >= j1)
    j2 += 1; # statistically shifts from j1,...,N_people-1 to j1+1, ..., N_people
  endif

  ## choose who wins
  win = randi(2);
  ## set the fraction
  if(ran_frac)
    f = rand(1) .* frac; # uniform random distribution
  else
    f = frac; # constant
  endif
  ## the wager is frac times the lower amount of cash of the two people
  wager = f * min(cash(j1),cash(j2));
  if(win == 1)
    cash(j1) += wager;
    cash(j2) -= wager;
  else
    cash(j2) += wager;
    cash(j1) -= wager;
  endif
endfunction

## This function updates all pairs, chosen randomly, simultaneously
function result = update_all()
  global cash
  global N_people
  global frac
  global ran_frac

  if(2*int64(N_people/2) != N_people)
    printf("Error N_people = %d is not even.\n",N_people)
    quit
  endif

  ## random permutation of all people
  id = randperm(N_people);

  ## set the fraction
  if(ran_frac)
    f = frac * rand(N_people/2,1); # uniform random distribution
  else
    f = frac * ones(N_people/2,1); # constant
  endif

  for j=1:(N_people/2)
    j1=j;
    j2=j+N_people/2;

    ## choose who wins
    win = randi(2);
    ## the wager is frac times the lower amount of cash of the two people
    wager = f(j) * min(cash(id(j1)),cash(id(j2)));
    if(win == 1)
      cash(id(j1)) += wager;
      cash(id(j2)) -= wager;
    else
      cash(id(j2)) += wager;
      cash(id(j1)) -= wager;
    endif
  endfor
endfunction

## Subtract a flat (proportional) tax from *everyone* (including the
## poorest, for simplicity!); and then add a flat (additive) basic universal income
## to everybody; this follows McGhee's definition of the model.
function result = tax_all()
  global cash
  global N_people
  global tax_frac

  cash .*= (1.0-tax_frac);
  tax_per_person = tax_frac * sum(cash) /N_people;
  cash .+= tax_per_person;
endfunction

## Calculate the Gini coefficient https://en.wikipedia.org/wiki/Gini_coefficient
function gini = get_Gini()
  global cash
  global N_people

  m = mean(cash);
  c_rows = repmat(cash, N_people, 1); # identical copies of 'cash' as rows
  c_cols = repmat(cash', 1, N_people); # identical copies of 'cash' as columns
  double_sum = sum(sum(abs(c_rows .- c_cols)));
  gini = double_sum / (2.0 * N_people^2 * m);
endfunction


graphics_toolkit gnuplot

for i=1:N_iterations
  ## Main model in https://pudding.cool/2022/12/yard-sale :
  update_all()

  ##update_pairs() # This is much slower than the model in https://pudding.cool/2022/12/yard-sale .

  ## Optionally add taxation as in https://pudding.cool/2022/12/yard-sale ;
  ## there will still be great wealth inequality, but without the extreme megarich tail:
  if(do_tax)
    tax_all()
  endif

  ##printf("try i=%d ",i)
  N_outputs = 200;
  gini(i) = get_Gini();
  if(mod(i,int64(N_iterations/N_outputs)) == 0) # display a histogram N_outputs times in total
    hist(cash,50)
    s = sprintf("Gini = %7.3f", gini(i));
    title(s)
    pause(0.0001)
  endif
endfor

## TODO: you can plot the evolution of gini(i),
### e.g. plot(1:N_iterations,gini,'-') ...
