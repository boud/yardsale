# yardsale

Yard-sale simulation for Chakraborti 2002 affine wealth model

# Run

This should run with GNU Octave 6.2.0 - https://docs.octave.org/v6.2.0/Script-Files.html

````
octave --no-gui --persist yardsale.m
````

Try changing from `do_tax = 0` to `do_tax = 1` and see the difference
in the Gini coefficient shown in the title of the graph. Without the
tax the Gini coefficient grows to about 0.985 - almost perfect
oligarchy; but with the tax it only grows to around 0.58 and then
stabilises between roughly 0.57 and 0.59.

# Sources:
* McGhee, Michelle et al 2022 - very clear pop-science explanation - https://pudding.cool/2022/12/yard-sale
* Chakraborti, Anirban 2002 - https://www.worldscientific.com/doi/10.1142/S0129183102003905
* Boghosian, Bruce M. 2019 - affine wealth model https://www.scientificamerican.com/article/is-inequality-inevitable/
* https://en.wikipedia.org/wiki/Bruce_M._Boghosian


# GNU GPL-3+

Copyright (C) 2022 Boud Roukema GPL-3+

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

See also https://www.gnu.org/licenses/gpl.html
